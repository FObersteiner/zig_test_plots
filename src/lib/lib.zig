const std = @import("std");

pub fn mean(T: type, arr: []T) T {
    var sum: T = 0;
    for (arr) |v| sum += v;
    return if (0 < arr.len) sum / arr.len else 0;
}

pub fn variance(T: type, arr: []T) T {
    if (arr.len <= 1) return 0;
    const avg: T = mean(T, arr);
    var _variance: T = 0;
    for (arr) |v| {
        const elem = if (v < avg) avg - v else v - avg;
        _variance += elem * elem;
    }
    return _variance;
}

pub fn stddev(T: type, arr: []T) T {
    if (arr.len <= 1) return 0;
    const _var: T = variance(T, arr);
    return std.math.sqrt(_var / (arr.len - 1));
}

test "mean" {
    std.testing.expectEqual(mean([5]u8{ 1, 2, 3, 4, 5 }), @as(u8, 3));
}

test "variance" {
    std.testing.expectEqual(variance([5]u8{ 1, 2, 3, 4, 5, 7, 8 }), @as(u8, 6));
}

test "stddev" {
    std.testing.expectEqual(stddev([5]u8{ 1, 2, 3 }), @as(u8, 1));
}
