const std = @import("std");
const zigplotlib = @import("zigplotlib");
const lib = @import("./lib/lib.zig");
const SVG = zigplotlib.SVG;
const rgb = zigplotlib.rgb;
const Range = zigplotlib.Range;
const Figure = zigplotlib.Figure;
const Line = zigplotlib.Line;

const SMOOTHING = 0.2;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    var t = try std.time.Timer.start();

    const n: usize = 10000;

    var noop_times: [n]u64 = undefined;
    var i: usize = 0;
    while (i < n) : (i += 1) {
        noop_times[i] = t.lap();
        //noop_times[i] = t.read();
        // t.reset();
    }

    var x: [n]f32 = undefined;
    var y: [n]f32 = undefined;

    for (noop_times, 0..) |_t, idx| {
        x[idx] = @floatFromInt(idx);
        y[idx] = @floatFromInt(_t);
    }

    const avg: u64 = lib.mean(u64, &noop_times);
    const stdev: u64 = lib.stddev(u64, &noop_times);

    std.debug.print("n: {d}, avg: {d}, std: {d}\n", .{
        n,
        avg,
        stdev,
    });

    const n_f: f32 = @floatFromInt(n);
    var figure = Figure.init(allocator, .{
        .axis = .{
            //            .y_scale = .log,
            .tick_count_x = .{ .count = 3 },
            .x_range = Range(f32){ .min = 0.0, .max = n_f },
            .tick_count_y = .{ .count = 3 },
            .y_range = Range(f32){ .min = 0.0, .max = 250.0 },
        },
        // .value_padding = .{
        //     .x_min = .{ .value = 0.0 },
        //     .x_max = .{ .value = n_f },
        // },
        // .axis = .{
        //     .show_y_axis = true,
        // },
    });
    defer figure.deinit();

    try figure.addPlot(Line{
        .x = &x,
        .y = &y,
        .style = .{
            .color = rgb.BLUE,
            .width = 2.0,
            //.smooth = SMOOTHING,
        },
    });

    var svg = try figure.show();
    defer svg.deinit();

    // Write to an output file (out.svg)
    var file = try std.fs.cwd().createFile("line.svg", .{});
    defer file.close();

    try svg.writeTo(file.writer());
}
